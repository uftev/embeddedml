import re
import torch
import numpy as np
import copy
from typing import Dict

def l1_structured_pruning(s: Dict, prune_ratio: float) -> Dict:
    s = copy.deepcopy(s)
    print(s.keys())
    for key in s:
        match = re.match(r"conv(\d)\.weight", key)
        if not match or match.group(1) == "1" or match.group(1) == "9":
            continue
        idx = int(match.group(1))
        tensor = s[key]

        l2s = []

        for i, subtens in enumerate(tensor):
            l2s.append(torch.sum(torch.abs(subtens)))

        perc = np.percentile(l2s, prune_ratio * 100)

        bias = s[f"conv{idx}.bias"]

        for i, subtensor in enumerate(tensor):
            if l2s[i] < perc:
                tensor[i, :] = torch.zeros(tensor[i, :].shape)
                bias[i] = 0.0

    return s


def densify_state_dict(s: Dict) -> Dict:
    keep = None
    last_len = 0
    s = copy.deepcopy(s)
    for key in s:
        match = re.match(r"conv(\d)\.weight", key)
        if match and match.group(1) != "1" and match.group(1) != "9":

            idx = int(match.group(1))
            tensor = s[key]
            if keep is not None:
                tensor = tensor[:, keep, :, :]
            keep = []
            for i, subtens in enumerate(tensor):
                if not torch.all(subtens == 0):
                    keep.append(i)
            last_len = len(tensor)
            s[key] = tensor[keep, :, :, :]
            s[f"conv{idx}.bias"] = s[f"conv{idx}.bias"][keep]

    keep.sort()
    s["conv9.weight"] = s["conv9.weight"][:,keep, :, :]
    return s


def prune_and_densify(s: Dict, prune_ratio: float) -> Dict:
    s = l1_structured_pruning(s, prune_ratio)
    s = densify_state_dict(s)
    return s
