import torch
import torch.nn as nn


def fuse_conv_and_bn(conv, bn):
    fused_conv = nn.Conv2d(conv.in_channels,
                           conv.out_channels,
                           kernel_size=conv.kernel_size,
                           stride=conv.stride,
                           padding=conv.padding,
                           bias=True)

    with torch.no_grad():

        w_conv = conv.weight.clone().view(conv.out_channels, -1)
        w_bn = torch.diag(bn.weight.div(torch.sqrt(bn.eps + bn.running_var)))

        fused_conv.weight.copy_(torch.mm(w_bn, w_conv).view(fused_conv.weight.size()))

        if conv.bias is None:
            b_conv = torch.zeros(conv.weight.size(0))
        else:
            b_conv = conv.bias

        b_bn = bn.bias - bn.weight.mul(bn.running_mean).div(torch.sqrt(bn.running_var + bn.eps))

        fused_conv.bias.copy_(b_conv + b_bn)

    return fused_conv
